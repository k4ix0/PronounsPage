module.exports = {
    env: {
        es2021: true,
        node: true,
    },
    root: true,
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:import/recommended',
        'plugin:import/typescript',
        'plugin:jsonc/recommended-with-json',
        'plugin:json-schema-validator/recommended',
        'plugin:yml/standard',
        'plugin:vue/recommended',
    ],
    plugins: [
        '@stylistic',
        '@stylistic/ts',
    ],
    ignorePatterns: ['cache', 'census', 'data', 'dist', 'keys', 'new', 'static', 'pnpm-lock.yaml'],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    settings: {
        'import/resolver': {
            node: true,
            typescript: true,
        },
    },
    rules: {
        camelcase: ['warn', { properties: 'never' }],
        curly: 'warn',
        'dot-notation': 'warn',
        eqeqeq: 'warn',
        'no-constant-condition': 'warn',
        'no-empty': 'warn',
        'no-irregular-whitespace': 'warn',
        'no-prototype-builtins': 'warn',
        'no-template-curly-in-string': 'warn',
        'no-useless-escape': 'warn',
        'no-useless-rename': 'warn',
        'object-shorthand': 'warn',
        'prefer-const': 'warn',
        'prefer-template': 'warn',
        'import/extensions': ['error', 'always'],
        'import/consistent-type-specifier-style': ['warn', 'prefer-top-level'],
        'import/no-cycle': 'warn',
        'import/no-self-import': 'error',
        'import/no-useless-path-segments': 'error',
        '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_', varsIgnorePattern: '^_' }],
        '@typescript-eslint/no-explicit-any': 'warn',
        '@typescript-eslint/no-this-alias': 'warn',
        '@stylistic/array-bracket-newline': ['warn', 'consistent'],
        '@stylistic/array-bracket-spacing': 'warn',
        '@stylistic/array-element-newline': ['warn', 'consistent'],
        '@stylistic/arrow-parens': 'warn',
        '@stylistic/arrow-spacing': 'warn',
        '@stylistic/block-spacing': 'warn',
        '@stylistic/brace-style': 'warn',
        '@stylistic/comma-dangle': ['warn', 'always-multiline'],
        '@stylistic/comma-spacing': 'warn',
        '@stylistic/comma-style': 'warn',
        '@stylistic/computed-property-spacing': 'warn',
        '@stylistic/dot-location': ['warn', 'property'],
        '@stylistic/eol-last': 'warn',
        '@stylistic/function-call-argument-newline': ['warn', 'consistent'],
        '@stylistic/function-call-spacing': 'warn',
        '@stylistic/function-paren-newline': ['warn', 'multiline-arguments'],
        '@stylistic/implicit-arrow-linebreak': 'warn',
        '@stylistic/indent': ['warn', 4, { SwitchCase: 1 }],
        '@stylistic/key-spacing': 'warn',
        '@stylistic/keyword-spacing': 'warn',
        '@stylistic/linebreak-style': 'warn',
        '@stylistic/lines-between-class-members': ['warn', { enforce: [
            { blankLine: 'never', prev: 'field', next: 'field' },
            { blankLine: 'always', prev: '*', next: 'method' },
        ] }],
        '@stylistic/max-len': ['warn', { code: 120 }],
        '@stylistic/max-statements-per-line': 'warn',
        '@stylistic/multiline-ternary': ['warn', 'always-multiline'],
        '@stylistic/new-parens': 'warn',
        '@stylistic/newline-per-chained-call': 'warn',
        '@stylistic/no-extra-parens': 'warn',
        '@stylistic/no-extra-semi': 'warn',
        '@stylistic/no-multi-spaces': 'warn',
        '@stylistic/no-multiple-empty-lines': ['warn', { max: 2, maxEOF: 0, maxBOF: 0 }],
        '@stylistic/no-tabs': 'warn',
        '@stylistic/no-trailing-spaces': 'warn',
        '@stylistic/no-whitespace-before-property': 'warn',
        '@stylistic/object-curly-newline': 'warn',
        '@stylistic/object-curly-spacing': ['warn', 'always'],
        '@stylistic/object-property-newline': ['warn', { allowAllPropertiesOnSameLine: true }],
        '@stylistic/one-var-declaration-per-line': 'warn',
        '@stylistic/operator-linebreak': 'warn',
        '@stylistic/padded-blocks': ['warn', 'never'],
        '@stylistic/quote-props': ['warn', 'as-needed'],
        '@stylistic/quotes': ['warn', 'single'],
        '@stylistic/rest-spread-spacing': 'warn',
        '@stylistic/semi': 'warn',
        '@stylistic/semi-spacing': 'warn',
        '@stylistic/space-before-blocks': 'warn',
        '@stylistic/space-before-function-paren': ['warn', { named: 'never' }],
        '@stylistic/space-in-parens': 'warn',
        '@stylistic/space-infix-ops': 'warn',
        '@stylistic/space-unary-ops': 'warn',
        '@stylistic/spaced-comment': 'warn',
        '@stylistic/switch-colon-spacing': 'warn',
        '@stylistic/template-curly-spacing': 'warn',
        '@stylistic/template-tag-spacing': 'warn',
        '@stylistic/wrap-iife': 'warn',
        '@stylistic/yield-star-spacing': 'warn',
        'jsonc/array-bracket-newline': ['warn', 'consistent'],
        'jsonc/array-bracket-spacing': 'warn',
        'jsonc/array-element-newline': ['warn', 'consistent'],
        'jsonc/indent': 'warn',
        'jsonc/key-spacing': 'warn',
        'jsonc/no-irregular-whitespace': 'warn',
        'jsonc/object-curly-newline': 'warn',
        'jsonc/object-curly-spacing': ['warn', 'always'],
        'jsonc/object-property-newline': ['warn', { allowAllPropertiesOnSameLine: true }],
        'json-schema-validator/no-invalid': ['error', {
            schemas: [
                {
                    fileMatch: ['locale/*/config.suml'],
                    schema: 'locale/config.schema.json',
                },
            ],
        }],
        'yml/flow-mapping-curly-spacing': ['warn', 'always'],
        'yml/no-irregular-whitespace': 'off',
        'yml/plain-scalar': 'off',
        'yml/quotes': ['warn', { prefer: 'single' }],
        // these two rules do not work well with inline lists
        'yml/block-sequence-hyphen-indicator-newline': 'off',
        'yml/indent': 'off',
        'vue/first-attribute-linebreak': ['warn', { singleline: 'beside' }],
        'vue/html-indent': ['warn', 4],
        'vue/html-self-closing': ['warn', { html: { normal: 'never' } }],
        'vue/max-attributes-per-line': ['warn', { singleline: 5 }],
        'vue/multi-word-component-names': 'off',
        'vue/no-mutating-props': 'warn',
        'vue/require-v-for-key': 'warn',
        'vue/singleline-html-element-content-newline': ['warn', { externalIgnores: ['T', 'nuxt-link'] }],
        'vue/valid-template-root': 'warn',
        'vue/valid-v-for': 'warn',
        'vue/array-bracket-newline': ['warn', 'consistent'],
        'vue/array-bracket-spacing': 'warn',
        'vue/array-element-newline': ['warn', 'consistent'],
        'vue/arrow-spacing': 'warn',
        'vue/block-spacing': 'warn',
        'vue/brace-style': 'warn',
        'vue/camelcase': ['warn', { properties: 'never' }],
        'vue/comma-dangle': ['warn', 'always-multiline'],
        'vue/comma-spacing': 'warn',
        'vue/comma-style': 'warn',
        'vue/dot-location': ['warn', 'property'],
        'vue/dot-notation': 'warn',
        'vue/eqeqeq': 'warn',
        'vue/key-spacing': 'warn',
        'vue/keyword-spacing': 'warn',
        'vue/multiline-ternary': ['warn', 'always-multiline'],
        'vue/no-constant-condition': 'warn',
        'vue/no-empty-pattern': 'warn',
        'vue/no-extra-parens': 'warn',
        'vue/no-loss-of-precision': 'warn',
        'vue/no-sparse-arrays': 'warn',
        'vue/object-curly-newline': 'warn',
        'vue/object-curly-spacing': ['warn', 'always'],
        'vue/object-property-newline': ['warn', { allowAllPropertiesOnSameLine: true }],
        'vue/object-shorthand': 'warn',
        'vue/operator-linebreak': 'warn',
        'vue/prefer-template': 'warn',
        'vue/quote-props': ['warn', 'as-needed'],
        'vue/space-in-parens': 'warn',
        'vue/space-infix-ops': 'warn',
        'vue/space-unary-ops': 'warn',
        'vue/template-curly-spacing': 'warn',
    },
    overrides: [
        {
            files: ['*.ts', '*.vue'],
            rules: {
                // rules which require type information need to be listed separately
                '@typescript-eslint/await-thenable': 'error',
                '@typescript-eslint/adjacent-overload-signatures': 'warn',
                '@typescript-eslint/array-type': 'warn',
                '@typescript-eslint/consistent-indexed-object-style': 'warn',
                '@typescript-eslint/consistent-type-assertions': 'warn',
                '@typescript-eslint/consistent-type-definitions': 'warn',
                '@typescript-eslint/consistent-type-imports': 'warn',
                '@typescript-eslint/consistent-type-exports': 'warn',
                '@typescript-eslint/explicit-function-return-type': 'warn',
                '@typescript-eslint/no-unnecessary-boolean-literal-compare': 'warn',
                '@typescript-eslint/no-unnecessary-type-arguments': 'warn',
                '@typescript-eslint/no-unnecessary-type-assertion': 'warn',
                '@stylistic/ts/type-annotation-spacing': 'warn',
            },
            parser: '@typescript-eslint/parser',
            parserOptions: {
                project: true,
                tsconfigRootDir: __dirname,
            },
        },
        {
            files: ['*.d.ts'],
            rules: {
                // var is needed for globalThis declaration
                'no-var': 'off',
            },
        },
        {
            files: ['*.json'],
            parser: 'jsonc-eslint-parser',
            rules: {
                '@stylistic/max-len': 'off',
            },
        },
        {
            files: ['tsconfig.json'],
            rules: {
                'jsonc/no-comments': 'off',
            },
        },
        {
            files: ['*.suml', '*.yml'],
            parser: 'yaml-eslint-parser',
            rules: {
                '@stylistic/spaced-comment': 'off',
            },
        },
        {
            files: ['*.vue'],
            parser: 'vue-eslint-parser',
            parserOptions: {
                parser: '@typescript-eslint/parser',
                extraFileExtensions: ['.vue'],
            },
        },
    ],
};
