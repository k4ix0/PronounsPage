import type { Config } from '../../locale/config.ts';

export const configWithPronouns: Config = {
    locale: 'en',
    header: true,
    pronouns: {
        enabled: true,
        route: 'pronouns',
        default: 'he',
        any: 'any',
        plurals: true,
        honorifics: false,
        generator: {
            enabled: true,
            slashes: false,
        },
        multiple: {
            name: 'Interchangeable forms',
            description: '…',
            examples: ['he&she'],
        },
        null: false,
        emoji: false,
    },
    sources: {
        enabled: false,
    },
    nouns: {
        enabled: false,
    },
    inclusive: {
        enabled: false,
    },
    terminology: {
        enabled: false,
    },
    names: {
        enabled: false,
    },
    people: {
        enabled: false,
    },
    english: {
        enabled: false,
    },
    faq: {
        enabled: false,
    },
    links: {
        enabled: false,
    },
    contact: {
        enabled: false,
    },
    support: {
        enabled: false,
    },
    user: {
        enabled: false,
    },
    profile: {
        enabled: false,
    },
    census: {
        enabled: false,
    },
    redirects: [],
};
