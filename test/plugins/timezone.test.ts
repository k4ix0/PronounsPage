/**
 * @jest-environment jsdom
 */

import { beforeAll, describe, expect, jest, test } from '@jest/globals';
import { mount } from '@vue/test-utils';
import { DateTime } from 'luxon';

import timezone from '../../plugins/timezone.ts';

describe('timezone info', () => {
    const wrapper = mount(timezone, {
        render(h) {
            return h('span');
        },
    });

    beforeAll(() => {
        jest.spyOn(DateTime, 'local').mockReturnValue(DateTime.fromISO('2024-01-01T10:00:00'));
    });

    test('for known timezone displays offset', () => {
        const timezoneInfo = wrapper.vm.getTimezoneInfo('America/New_York');
        expect(timezoneInfo).toEqual({
            area: 'America',
            displayLocation: 'New York',
            icon: 'globe-americas',
            location: 'New York',
            long: 'Eastern Standard Time',
            offset: -300,
            offsetFormatted: '-05:00',
            short: 'EST',
            timezone: 'America/New_York',
        });
    });
    test('for known wrongly named timezone uses correct name', () => {
        const timezoneInfo = wrapper.vm.getTimezoneInfo('Europe/Kiev');
        expect(timezoneInfo).toEqual({
            area: 'Europe',
            displayLocation: 'Kyiv',
            icon: 'globe-europe',
            location: 'Kiev',
            long: 'Eastern European Standard Time',
            offset: 120,
            offsetFormatted: '+02:00',
            short: 'GMT+2',
            timezone: 'Europe/Kiev',
        });
    });
    test('for unknown timezone returns null', () => {
        const timezoneInfo = wrapper.vm.getTimezoneInfo('Etc/Unknown');
        expect(timezoneInfo).toBeNull();
    });
});
