import { describe, expect, test } from '@jest/globals';

import { normalizeUrl } from '../../plugins/track.ts';

describe('when tracking', () => {
    const base = 'https://pronouns.page';

    test('normal pages are tracked verbatim', () => {
        expect(normalizeUrl(new URL('/pronouns', base))).toEqual(new URL('/pronouns', base));
    });
    test.each([
        { given: '/@example', expected: '/@--redacted--' },
        { given: '/u/example', expected: '/u/--redacted--' },
        { given: '/card/example', expected: '/card/--redacted--' },
    ])('pages containing user name are redacted', ({ given, expected }) => {
        expect(normalizeUrl(new URL(given, base))).toEqual(new URL(expected, base));
    });
    test.each([
        { given: '/terminology#queer', expected: '/terminology' },
        { given: '/@example#they/them', expected: '/@--redacted--' },
    ])('hashes are stripped', ({ given, expected }) => {
        expect(normalizeUrl(new URL(given, base))).toEqual(new URL(expected, base));
    });
});
