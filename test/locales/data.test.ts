import { describe, expect, test } from '@jest/globals';

import allLocales from '../../locale/locales.ts';
import { loadTsv } from '../../src/tsv.ts';
import { Example } from '../../src/classes.ts';
import type { ExpectationResult } from 'expect';
import { loadSumlFromBase } from '../../server/loader.ts';
import type { Config } from '../../locale/config.ts';
import type { PronounExamplesData, PronounsData } from '../../plugins/data';

const __dirname = new URL('.', import.meta.url).pathname;

function toHaveValidMorphemes(actual: string, morphemes: string[]): ExpectationResult {
    const containedMorphemes = Example.parse(actual).filter((part) => part.variable)
        .map((part) => part.str.replace(/^'/, ''));
    const unknownMorphemes = containedMorphemes.filter((morpheme) => !morphemes.includes(morpheme));
    if (unknownMorphemes.length > 0) {
        return {
            message: () => `expected example '${actual}' to have valid morphemes,` +
                ` but these are unknown:\n${unknownMorphemes.join(', ')}`,
            pass: false,
        };
    } else {
        return {
            message: () => 'expected example to have invalid morphemes',
            pass: true,
        };
    }
}

declare module 'expect' {
    interface Matchers<R> {
        toHaveValidMorphemes(morphemes: string[]): R;
    }
}

expect.extend({ toHaveValidMorphemes });

describe.each(allLocales)('data files of $code', ({ code }) => {
    const config = loadSumlFromBase(`locale/${code}/config`) as Config;

    const examples = loadTsv<PronounExamplesData>(`${__dirname}/../../locale/${code}/pronouns/examples.tsv`);

    test('pronouns/pronouns.tsv match schema', async () => {
        const { default: MORPHEMES } = await import(`../../locale/${code}/pronouns/morphemes.js`);
        const pronouns = loadTsv<PronounsData<string[]>>(`${__dirname}/../../locale/${code}/pronouns/pronouns.tsv`);
        if (pronouns.length === 0) {
            return;
        }
        const required = [
            'key',
            'description',
            'normative',
            'plural',
            'pluralHonorific',
            'pronounceable',
            ...MORPHEMES,
        ];
        const optional = ['history', 'thirdForm', 'smallForm', 'sourcesInfo', 'hidden'];
        const actual = Object.keys(pronouns[0]);
        expect(actual).toEqual(expect.arrayContaining(required));
        expect([...required, ...optional]).toEqual(expect.arrayContaining(actual));
    });
    test('pronouns/examples.tsv match schema', () => {
        if (examples.length === 0) {
            return;
        }
        const required = [
            'singular',
        ];
        if (config.pronouns.plurals) {
            required.push('plural');
        }
        if (config.pronouns.honorifics) {
            required.push('isHonorific');
        }
        const actual = Object.keys(examples[0]);
        expect(actual).toEqual(required);
    });
    test('pronouns/examples.tsv contain valid morphemes', async () => {
        const { default: MORPHEMES } = await import(`../../locale/${code}/pronouns/morphemes.js`);
        for (const example of examples) {
            expect(example.singular).toHaveValidMorphemes(MORPHEMES);
            if (example.plural) {
                expect(example.plural).toHaveValidMorphemes(MORPHEMES);
            }
        }
    });
    test('pronouns/examples.tsv contains plural examples when language has plurals', () => {
        const hasExamplesWithPlurals = examples.some((example) => {
            return example.plural && example.plural !== example.singular;
        });
        expect(hasExamplesWithPlurals).toBe(!!config.pronouns.plurals);
    });
    test('pronouns/examples.tsv contains honorific examples when language has honorifics', () => {
        const hasExamplesWithHonorifics = examples.some((example) => example.isHonorific);
        expect(hasExamplesWithHonorifics).toBe(!!config.pronouns.honorifics);
    });
});
