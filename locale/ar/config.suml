locale: 'ar'
dir: 'rtl'

header: true

pronouns:
    enabled: true
    route: 'الضمائر'
    default: 'هو'
    any: 'أيّ ضمائر'
    plurals: false
    honorifics: false
    generator:
        enabled: false
    multiple:
        name: 'الضمائر القابلة للتبديل'
        description: >
            يستخدم العديد من الأشخاص غير الثنائيين أكثر من ضمير واحد بشكل قابل للتبديل
            ولا يمانعون إذا استدعيت عليهم بأي من الضمائر الذين اختاروها
            ويحب البعض عندما يُستعمل الضمائر بالتناوب عند الحديث
        examples: ['هو&هي', 'هم&هو', 'هم&هي']
    null: false
    emoji: false
    mirror:
        route: 'مرآة'
        name: 'ضمائر المرآة'
        description: >
            الشخص الذي يستخدم ضمائر المرآة يريد أن يشار إليه بنفس الضمائر الذي يستخدمها الشخص الذي يتكلم.
        example:
            - 'الشخص (أ) يستخدم ضمائر المرآة.'
            - 'الشخص (ب) تستخدم {/هي=هي/لها}، لذا عندما تتحدث عن الشخص (أ) تستخدم  الضمائر "هي/لها" للإشارة إليها.'
            - 'الشخص (ج) يستخدم {/هو=هو/له}، لذا عندما يتحدث عن الشخص (أ) يستخدم  الضمائر "هو/له" للإشارة إليه.'
    others: 'الضمائر الأخرى'
    threeForms: true

pronunciation:
    enabled: true
    voices:
        AR:
            language: 'arb'
            voice: 'Zeina'
            engine: 'standard'
        AE:
            language: 'ar-AE'
            voice: 'Hala'
            engine: 'neural'

sources:
    enabled: false
    route: 'المصادر'
    submit: true
    mergePronouns: {}
    extraTypes: ['الإبتعاد عن', 'nounself']

nouns:
    enabled: true
    route: 'القاموس'
    collapsable: false
    plurals: true
    pluralsRequired: false
    declension: false
    submit: true
    templates: false

community:
    route: 'المصطلحات'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: false
    categories:
        - 'التوجه الجنسي'
        - 'التوجه الرومانسي'
        - 'التوجه العالي'
        - 'الجنس'
        - 'التعبير الجنسي'
        - 'نموذج العلاقة'
        - 'اللغة'
        - 'الانجذاب'
        - 'السياسة'
        - 'الحكم المسبق'

    route: 'المصطلحات'

names:
    enabled: false

people:
    enabled: false

# optional, but would be nice to have
english:
    enabled: false
    route: 'english'

faq:
    enabled: true
    route: 'الأسئلة الشائعة'

links:
    enabled: true
    split: false
    route: 'الروابط'
    blogRoute: 'المدونة'
    links:
        -
            icon: 'globe-europe'
            url: 'https://web.archive.org/web/20220928213020/http://pronoun.is/'
            headline: 'Pronoun.is'
            extra: '- مصدر إلهام هذا الموقع'
        -
            icon: 'file-alt'
            url: 'https://doi.org/10.20378/irb-50040'
            lang: ['de']
            headline: 'Geschlechtergerechte Sprache im Arabischen'
            extra: 'حياد الجنس في اللغة العربية'
    academic: {}
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false
    zine:
        enabled: false

contact:
    enabled: true
    route: 'التواصل'
    team:
        enabled: true
        route: 'الجماعية'

support:
    enabled: true

user:
    enabled: true
    route: 'الحساب'
    termsRoute: 'الشروط'
    privacyRoute: 'الخصوصية'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            header: 'الألقاب'
            values: ['[من غير لقب]', 'السيد', 'السيدة', 'سيدِّي', 'سيدَّتي', 'الباشا', 'الأفندي']
        -
            header: 'الوصف'
            values: ['شخص', 'رجل', 'امرأة', 'بنت', 'صاح', 'ولد']
        -
            header: 'المجاملات'
            values: ['لطيف', 'لطيفة', 'جذاب', 'جذابة', 'حلو', 'حلوة']
        -
            header: 'العلاقات'
            values: ['صديقي', 'صديقتي', 'أخي', 'أختي', 'حبيبي', 'حبيبتي', 'زوج', 'زوجة']
    flags:
        defaultPronoun: 'هو'

calendar:
    enabled: true
    route: 'اليومية'

census:
    enabled: false

redirects: []

api: ~
