import { Event, day, EventLevel } from '~/src/calendar/helpers.ts';

export default [
    new Event('Abieluvõrdsuse päev (Eesti)', '_hrc', 6, day(20), EventLevel.Day, [], null, null, (y) => y >= 2023),
];
