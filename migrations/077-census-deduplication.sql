-- Up

CREATE TABLE census_deduplication (
    locale TEXT NOT NULL,
    edition TEXT NOT NULL,
    userId TEXT NULL,
    fingerprint TEXT NULL
);

INSERT INTO census_deduplication (locale, edition, userId, fingerprint)
SELECT locale, edition, userId, fingerprint FROM census WHERE userId IS NOT NULL OR fingerprint IS NOT NULL;

UPDATE census SET userId = null, fingerprint = null;

-- Down
