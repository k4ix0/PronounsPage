// load this module before other modules which require these global variables
import { loadSuml } from './loader.ts';

global.config = loadSuml('config');
global.translations = loadSuml('translations');
