import SQL from 'sql-template-strings';
import { ulid } from 'ulid';
import type { Request } from 'express';
import * as Sentry from '@sentry/node';

export default async (
    req: Pick<Request, 'db' | 'user' | 'rawUser'>,
    event: string,
    payload: object | null = null,
): Promise<void> => {
    try {
        const user = req.user || req.rawUser || { id: null, username: null };
        await req.db.get(SQL`INSERT INTO audit_log (id, userId, username, event, payload) VALUES (
            ${ulid()}, ${user.id}, ${user.username}, ${event}, ${payload ? JSON.stringify(payload) : null}
        )`);
    } catch (error) {
        Sentry.captureException(error);
    }
};
