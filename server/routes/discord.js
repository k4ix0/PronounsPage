import { Router } from 'express';
import { handleErrorAsync } from '../../src/helpers.ts';
import SQL from 'sql-template-strings';
import { normalise } from './user.js';

const configs = new Map()
    .set('DiscordToken', process.env.DISCORD_TOKEN)
    .set('DiscordClientId', process.env.DISCORD_CLIENT_ID)
    .set('DiscordClientSecret', process.env.DISCORD_CLIENT_SECRET)
    .set('DiscordRedirectUri', process.env.DISCORD_REDIRECT_URI);

const store = new Map();

const BASE_DISCORD_URI = 'https://discord.com/api/v10';

const OAUTH_URL = Object.assign(new URL('/oauth2/authorize', BASE_DISCORD_URI), {
    search: new URLSearchParams({
        client_id: configs.get('DiscordClientId'),
        redirect_uri: configs.get('DiscordRedirectUri'),
        response_type: 'code',
        scope: [
            'role_connections.write',
            'identify',
            'email',
        ].join(' '),
        prompt: 'consent',
    }).toString(),
}).toString();

class TokenSet {
    constructor(data) {
        this.access_token = data.access_token;
        this.refresh_token = data.refresh_token;
        this.expires_in = data.expires_in;
    }

    set(token, value) {
        this[token] = value;
        return this;
    }
}

class Request {
    constructor(grantType, miscConfig) {
        this.client_id = configs.get('DiscordClientId');
        this.client_secret = configs.get('DiscordClientSecret');
        this.grant_type = grantType;
        Object.assign(this, miscConfig);
    }
}

const getOAuthTokens = async (accessCode) => new TokenSet(
    await fetch(`${BASE_DISCORD_URI}/oauth2/token`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams(
            new Request('authorization_code', {
                code: accessCode,
                redirect_uri: configs.get('DiscordRedirectUri'),
            }),
        ),
    }).then((res) => res.json()),
);

const getAccessToken = async (userId) => {
    const tokenSet = store.get(userId);
    if (Date.now() > tokenSet.expires_in) {
        store.set(
            userId,
            new TokenSet(
                await fetch(`${BASE_DISCORD_URI}/oauth2/token`, {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    body: new URLSearchParams(
                        new Request('refresh_token', { refresh_token: tokenSet.refresh_token }),
                    ),
                }).then((res) => res.json()),
            ).set('expires_at', Date.now() + tokenSet.expires_in * 1e3),
        );
    }
    return tokenSet.access_token;
};

const getUserData = async (token) => {
    const data = await fetch(`${BASE_DISCORD_URI}/users/@me`, {
        headers: { Authorization: `Bearer ${token.access_token}` },
    }).then((res) => res.json());
    return {
        uid: data.id,
        email: data.email,
        username: data.username,
    };
};

const updateMetadata = async (req, res) => {
    const metaData = new Map()
        .set('isTeam', false);
    const connectedUser = await req.db.get(
        SQL`
        SELECT users.username FROM social_lookup LEFT JOIN users on social_lookup.userId = users.id
        WHERE social_lookup.provider = 'discord' AND social_lookup.identifier = ${req.data.uid};
        `,
    );
    const user = await req.db.get(
        SQL`
        SELECT username, roles FROM users
        WHERE username = ${normalise(connectedUser.username)} OR email = ${req.data.email}
        LIMIT 1;
        `,
    );
    if (!user) {
        const error = {
            code: 404,
            step: 1,
            error: 'User not found',
            hint: [
                'Try linking your Discord account to your Pronouns.Page account first.\n\n',
                'After that, on your account settings, select "Login Methods", and ensure the slider saying:',
                '"Allow getting my profile looked up by social media handles / identifiers',
                '(it\'s useful for integrations, discord bots, browser extensions, …)" is enabled.\n\n',
                'If the issue persists, please contact support at support@pronouns.page',
            ].join(' '),
        };
        return res.status(error.code).json(error);
    }
    metaData.set('isTeam', user.roles !== '');
    await fetch(
        `${BASE_DISCORD_URI}/users/@me/applications/${configs.get('DiscordClientId')}/role-connection`,
        {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessToken(req.uid)}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                platform_name: 'Pronouns.Page',
                metadata: Object.fromEntries(metaData.entries().map(
                    ([key, value]) => [key.toLowerCase(), value],
                )),
            }),
        },
    );
    res.send('200 OK');
};

const router = Router();

router.get('/discord/linked-role', async (req, res) => {
    res.redirect(OAUTH_URL);
});

router.get('/discord/oauth2', handleErrorAsync(async (req, res) => {
    const { code } = req.query;
    const
        token = await getOAuthTokens(code),
        userData = await getUserData(token);
    store.set(userData.uid, token.set('expires_at', Date.now() + token.expires_in * 1000));
    await updateMetadata(Object.assign(req, { data: userData }), res);
}));

export default router;
