declare module 'avris-columnist' {
    class Columnist {
        constructor(element: Element);

        start();
    }
    export default Columnist;
}
