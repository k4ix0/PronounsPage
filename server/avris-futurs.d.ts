declare module 'avris-futurus' {
    class Futurus {
        futuriseWord(word: string): string;

        futuriseText(text: string): string;
    }
    export default new Futurus();
}
