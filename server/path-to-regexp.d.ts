declare module 'path-to-regexp' {
    const pathToRegexp: (path: string) => RegExp;
    export = pathToRegexp;
}
