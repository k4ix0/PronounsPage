import fetch from 'node-fetch';
import { S3 } from '@aws-sdk/client-s3';
import { awsConfig, awsParams } from './aws.ts';
import md5 from 'js-md5';
import { loadImage, createCanvas } from 'canvas';

const s3 = new S3(awsConfig);

export default async (provider: string, url: string): Promise<string | null> => {
    if (!url) {
        return null;
    }

    const key = `images-copy/${provider}/${md5(url)}.png`;

    try {
        await s3.headObject({ Key: key, ...awsParams });

        return `${process.env.CLOUDFRONT}/${key}`;
    } catch {
        try {
            const image = await loadImage(Buffer.from(await (await fetch(url)).arrayBuffer()));
            const canvas = createCanvas(image.width, image.height);
            canvas.getContext('2d').drawImage(image, 0, 0);

            await s3.putObject({
                Key: key,
                Body: canvas.toBuffer('image/png'),
                ContentType: 'image/png',
                ACL: 'public-read',
                ...awsParams,
            });

            return `${process.env.CLOUDFRONT}/${key}`;
        } catch (e) {
            return null;
        }
    }
};
