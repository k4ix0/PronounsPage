import fs from 'fs';
import Suml from 'suml';

import { loadTsv as baseLoadTsv } from '../src/tsv.ts';

export const loadSumlFromBase = (name: string): unknown => new Suml().parse(fs.readFileSync(`./${name}.suml`, 'utf-8'));
export const loadSuml = (name: string): unknown => loadSumlFromBase(`data/${name}`);

export const loadTsv = <T = unknown>(name: string): T[] => baseLoadTsv(`./data/${name}.tsv`);
