import { Example, NounTemplate, PronounGroup, PronounLibrary, Person, NounDeclension } from './classes.ts';
import { buildDict, buildList } from './helpers.ts';
import { parsePronouns } from './buildPronoun.ts';
import config from '../data/config.suml';

import pronounsRaw from '../data/pronouns/pronouns.tsv';
export const pronouns = parsePronouns(config, pronounsRaw);

import examplesRaw from '../data/pronouns/examples.tsv';
export const examples = buildList(function* () {
    for (const e of examplesRaw) {
        yield new Example(
            Example.parse(e.singular),
            Example.parse(e.plural || e.singular),
            e.isHonorific,
        );
    }
});

import nounTemplatesRaw from '../data/nouns/nounTemplates.tsv';
export const nounTemplates = buildList(function* () {
    for (const t of nounTemplatesRaw) {
        yield NounTemplate.from(t);
    }
});

import pronounGroupsRaw from '../data/pronouns/pronounGroups.tsv';
export const pronounGroups = buildList(function* () {
    for (const g of pronounGroupsRaw) {
        yield new PronounGroup(
            g.name,
            g.pronouns ? g.pronouns.replace(/،/g, ',').split(',') : [],
            g.description,
            g.key || null,
            g.hidden ?? false,
        );
    }
});

export const pronounLibrary = new PronounLibrary(config, pronounGroups, pronouns);

import peopleRaw from '../data/people/people.tsv';
export const people = buildList(function* () {
    for (const p of peopleRaw) {
        yield new Person(
            p.name,
            p.description,
            p.pronouns.split(','),
            p.sources ? p.sources.split(',') : [],
        );
    }
});

import nounDeclensionTemplatesRaw from '../data/nouns/nounDeclension.tsv';
export const nounDeclensionTemplates = buildList(function* () {
    for (const d of nounDeclensionTemplatesRaw) {
        yield new NounDeclension(d);
    }
});

import abbreviationsRaw from '../data/nouns/abbr.tsv';
export const abbreviations = buildDict(function* () {
    for (const a of abbreviationsRaw) {
        yield [a.abbreviation, a.meaning];
    }
});
