import { fetchJson } from './fetchJson.js';

let census_groups = {};
let census_comparisons = {};

const mainPlusDetails = (dict, wide) => (_, keys, content) => {
    let selectedDict = {};
    if (keys === undefined) {
        selectedDict = dict;
    } else {
        for (const key of keys.substring(1).split(',')) {
            selectedDict[key] = dict[key];
        }
    }

    return `
        <div class="${wide ? 'wide-escape' : ''}">
            <p>${content.replace(/%group%/g, 'general').replace(/<iframe class="graph" /g, '<iframe class="graph border" ')}</p>${
    Object.keys(selectedDict).map((group) => `
                <details class="border mb-3">
                    <summary class="bg-light px-2 py-1" onclick="this.parentElement.querySelector('iframe.graph').contentDocument.location.reload()">${selectedDict[group]}</summary>
                    <div class="border-top p-md-3 bg-white">${content.replace(/%group%/g, group)}</div>
                </details>`)
        .join('\n')
}</div>`;
};

const generateToC = (content, translator) => (_) => {
    const tags = [];
    let curentLevel = 2;
    let needsClosing = false;
    for (let [, level, id, title] of content.matchAll(/<h([2-6]) id="([^"]+)">([^<]+)<\/h\1>/g)) {
        level = parseInt(level);
        while (level < curentLevel) {
            tags.push('</li>'); tags.push('</ul>'); curentLevel--;
        }
        while (level > curentLevel) {
            tags.push('<ul>'); curentLevel++; needsClosing = false;
        }
        if (needsClosing) {
            tags.push('</li>');
        }
        tags.push('<li>');
        tags.push(`<a href="#${id}">`);
        tags.push(title);
        tags.push('</a>');
        needsClosing = true;
    }
    while (curentLevel < 2) {
        tags.push('</li>'); tags.push('</ul>'); curentLevel--; needsClosing = false;
    }
    if (needsClosing) {
        tags.push('</li>');
    }

    return `
        <div class="alert alert-light border">
            <h2 class="h4"><span class="fal fa-list"></span> ${translator.translate('links.blogTOC')}</h2>
            <ul class="mb-0">${tags.join('')}</ul>
        </div>
    `;
};

const generateGallery = (_, itemsString) => {
    const items = JSON.parse(`{${itemsString.replace(/&quot;/g, '"').replace(/,\s*$/, '')}}`);

    const label = (alt) => {
        if (!alt.startsWith('! ')) {
            return '';
        }

        return `<p class="small mt-2">${alt.substring(2)}</p>`;
    };

    const cells = Object.entries(items).map(([src, alt]) => `
        <div class="col-6 col-lg-4 columnist-column mb-3">
            <a href="${src}" target="_blank" rel="noopener">
                <img src="${src}" alt="${alt.startsWith('! ') ? alt.substring(2) : alt}"/>
            </a>
            ${label(alt)}
        </div>
    `);

    return `<div class="row columnist-wall--disabled">${cells.join('')}</div>`;
};

export default async function parseMarkdown(markdown, translator) {
    let content = `<div>${
        markdown
            .replace(/<table>/g, '<div class="table-responsive"><table class="table table-striped small">')
            .replace(/<\/table>/g, '</table></div>')
            .replace(/<a href="http/g, '<a target="_blank" rel="noopener" href="http')
            .replace(/<p>{details=(.+?)}<\/p>(.+?)<p>{\/details}<\/p>/gms, '<details class="border mb-3"><summary class="bg-light p-3">$1</summary><div class="border-top p-3 bg-white">$2</div></details>')
            .replace(/<img (.*?)>/g, (_, attrs) => {
                let classNames = 'border';
                const m = attrs.match(/alt="\{(.*)\}/);
                if (m) {
                    classNames = m[1];
                    attrs = attrs.replace(/alt="{(.*)}/, 'alt="');
                }
                return `<div class="text-center"><img ${attrs} class="${classNames}" loading="lazy"></div>`;
            })
            .replace(/{favicon=(.+?)}/g, '<img src="https://$1" alt="Favicon" style="width: 1em; height: 1em;"/>')
            .replace(/{embed=\/\/(.+?)=(.+?)}/g, '<div style="position: relative;height: 0;padding-bottom: 56.25%;"><iframe src="https://$1" title="$2" allowfullscreen sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute;top: 0; left: 0;width: 100%;height: 100%;border:0;"></iframe></div>')
            .replace(/{graph=([^}]+)}/g, '<iframe class="graph" src="$1.html" loading="lazy"></iframe>')

            .replace(/<p>{set_census_groups=(.+?)}<\/p>/gms, (_, value) => {
                census_groups = JSON.parse(value.replace(/&quot;/g, '"'));
                return '';
            })
            .replace(/<p>{set_census_comparisons=(.+?)}<\/p>/gms, (_, value) => {
                census_comparisons = JSON.parse(value.replace(/&quot;/g, '"'));
                return '';
            })
            .replace(/<p>{census_groups(:.+?)?}<\/p>(.+?)<p>{\/census_groups}<\/p>/gms, mainPlusDetails(census_groups, false))
            .replace(/<p>{census_comparisons(:.+?)?}<\/p>(.+?)<p>{\/census_comparisons}<\/p>/gms, mainPlusDetails(census_comparisons, true))

            .replace(/{json=([^=}]+)=([^=}]+)}/g, (_, filename, key) => {
                try {
                    return fetchJson(filename, key);
                } catch (e) {
                    // console.error(e);
                    return `<span class="badge bg-danger text-white">${e}</span>`;
                }
            })
            .replace(/<h1 id="🏳️🌈-/g, '<h1 id="') // license header
            .replace(/{wide_table}/g, '<div class="table-wide table-responsive my-5 headers-nowrap">')
            .replace(/{\/wide_table}/g, '</div>')
            .replace(/{gallery={(.*?)}}/gms, generateGallery)
    }</div>`;

    content = content.replace(/{table_of_contents}/g, generateToC(content, translator));

    content = content.replace(/{optional}(.+?){\/optional}/gms, (_, content) => {
        if (content.includes('badge bg-danger')) {
            return '';
        }
        return content;
    });

    const titleMatch = content.match('<h1[^>]*>(.+?)</h1>');
    const title = titleMatch ? titleMatch[1] : null;
    const imgMatch = content.match('<img src="([^"]+)"[^>]*>');
    const img = imgMatch ? imgMatch[1] : null;
    let intro = [];

    for (const introMatch of content.matchAll(/<p[^>]*>(.+?)<\/p>/gms)) {
        const p = introMatch[1].replace(/(<([^>]+)>)/ig, '').replace(/\s+/g, ' ');
        intro = [...intro, ...p.split(' ')];
    }

    return {
        title,
        img,
        intro: intro.length ? intro.slice(0, 24).join(' ') : null,
        content,
    };
}
