import dotenv from 'dotenv';

const __dirname = new URL('.', import.meta.url).pathname;

dotenv.config({ path: `${__dirname}/../.env` });
if (process.env.__INCLUDE) {
    dotenv.config({ path: `${__dirname}/../${process.env.__INCLUDE}` });
}
process.env.CLOUDFRONT = `https://${process.env.AWS_CLOUDFRONT_ID}.cloudfront.net`;
