export default (err, req) => {
    return `[${new Date().toISOString()}] [${req ? `${req.method} ${req.url}` : ''}] ${err.message || err} ${err.stack}`;
};
