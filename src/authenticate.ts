import jwt from './jwt.ts';
import type { Request } from 'express';
import type { User } from './user.ts';

export default ({ cookies, headers }: Request): User | undefined => {
    if (headers.authorization && headers.authorization.startsWith('Bearer ')) {
        return jwt.validate(headers.authorization.substring(7)) as User | undefined;
    }

    if (cookies.token && cookies.token !== 'null') {
        return jwt.validate(cookies.token) as User | undefined;
    }

    return undefined;
};
