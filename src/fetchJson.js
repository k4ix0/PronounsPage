let jsons_cache = undefined;

export const fetchJson = (filename, key) => {
    if (jsons_cache === undefined) {
        jsons_cache = JSON.parse(process.env.JSONS);
    }
    let c = jsons_cache[filename];
    // terrible conventions, i know… i'm so tired…
    let explicitSign = false;
    let absolute = false;
    if (key.startsWith('@')) {
        explicitSign = true;
        key = key.substring(1);
    }
    if (key.startsWith('^')) {
        absolute = true;
        key = key.substring(1);
    }
    for (let part of key.replace(/\\\./g, '£').split('.')) {
        part = part.replace(/&#39;/g, '\'').replace(/£/g, '.');
        c = c[part];
        if (c === undefined) {
            throw `item "${part}" is undefined (file ${filename})`;
        }
    }
    if (typeof c === 'number') {
        if (absolute) {
            c = Math.abs(c);
        }
        if (explicitSign && c > 0) {
            c = `+${c}`;
        }
        // TODO make generic
        c = c.toString().replace('.', ',');
    }
    return c;
};
