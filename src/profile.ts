import type { CustomEvent } from './calendar/helpers.ts';
import type { Opinion } from './opinions.ts';

export interface Profile {
    names: NameOpinion[];
    pronouns: ValueOpinion[];
    description: string;
    /**
     * @format date
     */
    birthday: string | null;
    timezone: Timezone | null;
    links: string[];
    flags: string[];
    customFlags: CustomFlag[];
    words: WordCategory[];
    teamName: string | null;
    footerName: string | null;
    footerAreas: string[];
    credentials: string[];
    credentialsLevel: number | null;
    credentialsName: string | null;
    opinions: Record<string, Opinion>;
    circle: RelatedPerson[];
    sensitive: string[];
    markdown: boolean;
    events: string[];
    customEvents: CustomEvent[];
}

interface ValueOpinion {
    value: string;
    opinion: string;
}

interface NameOpinion extends ValueOpinion {
    value: string;
    pronunciation: string;
    opinion: string;
}

export interface Timezone {
    tz: string;
    area: boolean;
    loc: boolean;
}

export interface WordCategory {
    header: string | null;
    values: ValueOpinion[];
}

interface CustomFlag {
    value: string;
    name: string;
    description: string | null;
    alt: string | null;
    link: string | null;
}

interface RelatedPerson {
    avatar: string;
    circleMutual: boolean;
    locale: string;
    relationship: string;
    username: string;
}
