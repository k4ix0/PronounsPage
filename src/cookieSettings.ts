import type { CookieSerializeOptions } from 'cookie';

export const sessionCookieSetting: CookieSerializeOptions = {
    sameSite: 'lax',
};

const inAYear = new Date();
inAYear.setFullYear(inAYear.getFullYear() + 1);

export const longtimeCookieSetting: CookieSerializeOptions = {
    ...sessionCookieSetting,
    // secure: process.env.NODE_ENV === 'production',
    expires: inAYear,
};
