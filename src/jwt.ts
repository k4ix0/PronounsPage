import jwt from 'jsonwebtoken';
import fs from 'fs';
import type { JwtPayload } from 'jsonwebtoken';
import * as Sentry from '@sentry/node';

const __dirname = new URL('.', import.meta.url).pathname;

class Jwt {
    private readonly privateKey: Buffer;
    private readonly publicKey: Buffer;

    constructor(privateKey: fs.PathOrFileDescriptor, publicKey: fs.PathOrFileDescriptor) {
        this.privateKey = fs.readFileSync(privateKey);
        this.publicKey = fs.readFileSync(publicKey);
    }

    sign(payload: string | object, expiresIn = '365d'): string {
        return jwt.sign(payload, this.privateKey, {
            expiresIn,
            algorithm: 'RS256',
            audience: process.env.ALL_LOCALES_URLS!.split(','),
            issuer: process.env.BASE_URL,
        });
    }

    validate(token: string): JwtPayload | string | undefined {
        try {
            return jwt.verify(token, this.publicKey, {
                algorithms: ['RS256'],
                audience: process.env.ALL_LOCALES_URLS!.split(','),
                issuer: process.env.ALL_LOCALES_URLS!.split(','),
            });
        } catch (error) {
            Sentry.captureException(error);
        }
    }
}

export default new Jwt(`${__dirname}/../keys/private.pem`, `${__dirname}/../keys/public.pem`);
