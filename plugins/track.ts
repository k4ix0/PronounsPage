import * as Sentry from '@sentry/vue';
import type { Plugin } from '@nuxt/types';

const USER_AT = /^\/@.+/;
const USER_SUBPAGE = /^\/(u|card)\/.*/;

export const normalizeUrl = (page: URL): URL => {
    if (USER_AT.test(page.pathname)) {
        page.pathname = page.pathname.replace(USER_AT, '/@--redacted--');
    }
    if (USER_SUBPAGE.test(page.pathname)) {
        page.pathname = page.pathname.replace(USER_SUBPAGE, '/$1/--redacted--');
    }
    page.hash = '';
    return page;
};

const plugin: Plugin = ({ app }) => {
    app.router?.afterEach((to) => {
        try {
            const url = normalizeUrl(new URL(to.fullPath, window.location.href));
            console.debug('[analytics] tracking page view:', url.toString());
            app.$plausible.trackPageview({
                url: url.toString(),
            });
        } catch (error) {
            Sentry.captureException(error);
        }
    });
};

export default plugin;
