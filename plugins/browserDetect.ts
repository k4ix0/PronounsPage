import type { Plugin } from '@nuxt/types';

const SAFARI_REGEX = /^((?!chrome|android).)*safari/i;

const plugin: Plugin = ({ req }, inject) => {
    inject('isSafari', () => {
        if (process.server && req) {
            return SAFARI_REGEX.test(req.headers['user-agent']!);
        }

        if (process.client) {
            return SAFARI_REGEX.test(window.navigator.userAgent);
        }

        return false;
    });
};

export default plugin;
