import Vue from 'vue';
import { sessionCookieSetting } from '../src/cookieSettings.ts';

export type Mode = 'light' | 'dark' | 'automatic';

export default Vue.extend({
    head() {
        if (this.$store.state.darkMode) {
            return {
                bodyAttrs: {
                    'data-theme': 'dark',
                    'data-bs-theme': 'dark',
                },
            };
        } else {
            return {};
        }
    },
    computed: {
        isDark: {
            get() {
                return this.$store.state.darkMode;
            },
            set(darkMode) {
                this.$store.commit('setDarkMode', darkMode);
                this.$cookies.set('darkMode', darkMode, sessionCookieSetting);
            },
        },
    },
    created() {
        this.$store.commit('setDarkMode', this.$cookies.get('darkMode'));
    },
    mounted() {
        this.isDark = this.detectDark();
    },
    methods: {
        getMode(): Mode {
            if (!process.client) {
                return 'automatic';
            }

            return localStorage.getItem('mode') as Mode | null || 'automatic';
        },
        detectDark(): boolean {
            if (!process.client) {
                return false;
            }

            switch (this.getMode()) {
                case 'light':
                    return false;
                case 'dark':
                    return true;
                case 'automatic':
                default:
                    return window.matchMedia('(prefers-color-scheme: dark)').matches;
            }
        },
        setMode(mode: Mode): void {
            if (!process.client) {
                return;
            }

            localStorage.setItem('mode', mode);
        },
    },
});
