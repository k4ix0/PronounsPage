import type { JwtPayload } from 'jsonwebtoken';
import type { Plugin } from '@nuxt/types';
import { isGranted, parseUserJwt } from '../src/helpers.ts';
import { longtimeCookieSetting } from '../src/cookieSettings.ts';
import type { User } from '../src/user.ts';

declare module 'vue/types/vue' {
    interface Vue {
        $user(): User | null;
        $isGranted(area?: string, locale?: string | null): boolean;
        $accounts(): void;
        $setToken(token: string): void;
        $removeToken(username?: string | null): void;
    }
}

const plugin: Plugin = ({ app, store }, inject) => {
    const token = app.$cookies.get('token');
    if (token) {
        store.commit('setToken', token);
        if (!store.state.token) {
            app.$cookies.removeAll();
        }
    }

    inject('user', (): User | null => store.state.user);
    inject('isGranted', (area = '', locale = null): boolean => {
        return !!store.state.user &&
            !!store.state.user.authenticated &&
            isGranted(store.state.user, locale || app.$config.locale, area)
        ;
    });

    const getAccounts = (fallback: string | null = null): Record<string, JwtPayload> => {
        const tokens = (window.localStorage.getItem('account-tokens') || fallback || '').split('|').filter((x) => !!x);
        const accounts: Record<string, JwtPayload> = {};
        for (const token of tokens) {
            const account = parseUserJwt(token);
            if (account !== null && typeof account !== 'string' && account.username && account.authenticated) {
                accounts[account.username] = { token, account };
            }
        }
        return accounts;
    };
    const saveAccounts = (accounts: Record<string, JwtPayload>): void => {
        store.commit('setAccounts', accounts);
        window.localStorage.setItem('account-tokens', Object.values(accounts).map((x) => x.token)
            .join('|'));
    };

    inject('accounts', (): void => {
        saveAccounts(getAccounts(store.state.token));
    });
    inject('setToken', (token: string): void => {
        const accounts = getAccounts();

        const usernameBefore = store.state.user?.username;

        store.commit('setToken', token);
        if (token) {
            const account = parseUserJwt(token) as JwtPayload;
            if (account.username && account.authenticated) {
                accounts[account.username] = { token, account };
            }
            app.$cookies.set('token', store.state.token, longtimeCookieSetting);
        } else {
            app.$cookies.remove('token');
        }
        saveAccounts(accounts);

        const usernameAfter = store.state.user?.username;

        if (usernameBefore !== usernameAfter) {
            const bc = new BroadcastChannel('account_switch');
            bc.postMessage(usernameAfter);
            bc.close();
        }
    });
    inject('removeToken', (username: string | null = null): void => {
        const accounts = getAccounts();

        if (store.state.user) {
            delete accounts[username || store.state.user.username];
        }
        if (!username) {
            if (Object.keys(accounts).length === 0) {
                store.commit('setToken', null);
                app.$cookies.removeAll();
            } else {
                store.commit('setToken', Object.values(accounts)[0].token);
                app.$cookies.set('token', store.state.token, longtimeCookieSetting);
            }
        }
        saveAccounts(accounts);
    });
};

export default plugin;
