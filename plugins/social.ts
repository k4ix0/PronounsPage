import Vue from 'vue';

export default Vue.extend({
    methods: {
        buildSocialLoginConnectLink(
            locale: string,
            provider: string,
            providerOptions: Record<string, any>,
            instance: string | null = null,
        ): string {
            const url = providerOptions.redirectViaHome
                ? new URL(`/api/user/social-redirect/${provider}/${locale}`, process.env.HOME_URL)
                : new URL(`/api/connect/${provider}`, process.env.BASE_URL);

            if (providerOptions.instanceRequired && instance) {
                url.searchParams.append('instance', instance ?? '');
            }

            if (providerOptions.redirectViaHome && this.$store.state.token) {
                // if a logged in user on a domain that's not *.pronouns.page connects a new oauth account,
                // home domain doesn't have the token (cross-domain cookie) and instead of connecting
                // we create a brand new account. let's just pass the token to it
                url.searchParams.append('token', this.$store.state.token);
            }

            return url.toString();
        },
    },
});
