import Vue from 'vue';

import { pronounLibrary, pronouns } from '../src/data.ts';
import { buildPronoun } from '../src/buildPronoun.ts';
import opinions from '../src/opinions.ts';
import type { Pronoun } from '../src/classes.ts';
import type { Profile } from '../src/profile.ts';

export interface PronounOpinion {
    link: string;
    pronoun: Pronoun | string;
    opinion: string;
}

export default Vue.extend({
    computed: {
        pronounOpinions(this: Vue & { profile: Profile }): PronounOpinion[] {
            const pronounOpinions: PronounOpinion[] = [];
            if (!this.profile) {
                return pronounOpinions;
            }
            for (const { value: pronoun, opinion } of this.profile.pronouns) {
                let link = pronoun
                    .trim()
                    .replace(new RegExp(`^${this.$base}`), '')
                    .replace(new RegExp(`^${this.$base.replace(/^https?:\/\//, '')}`), '')
                    .replace(new RegExp('^/'), '');

                try {
                    link = decodeURIComponent(link);
                } catch {
                    continue;
                }

                if (!link.startsWith(':') && this.$config.locale !== 'tok') {
                    link = link.toLowerCase();
                }

                const linkNorm = link.toLowerCase();
                if (linkNorm === this.$config.pronouns.any) {
                    pronounOpinions.push({
                        link,
                        pronoun: this.$t('pronouns.any.short'),
                        opinion,
                    });
                    continue;
                }

                const prefix = `${this.$config.pronouns.any}:`;
                if (linkNorm.startsWith(prefix)) {
                    const merged = pronounLibrary.byKey()[linkNorm.substring(prefix.length)];
                    if (merged) {
                        pronounOpinions.push({
                            link,
                            pronoun: merged.short(this.$translator),
                            opinion,
                        });
                        continue;
                    }
                }

                if (this.$config.pronouns.null && this.$config.pronouns.null.routes && this.$config.pronouns.null.routes.includes(linkNorm) ||
                    this.$config.pronouns.mirror && this.$config.pronouns.mirror.route === linkNorm
                ) {
                    pronounOpinions.push({
                        link,
                        pronoun: link.replace(/:+/g, ' '),
                        opinion,
                    });
                    continue;
                }

                const pronounEntity = buildPronoun(pronouns, link, this.$config, this.$translator);

                if (pronounEntity) {
                    pronounOpinions.push({
                        link,
                        pronoun: pronounEntity,
                        opinion,
                    });
                }
            }
            return pronounOpinions;
        },
        mainPronoun(): Pronoun | null {
            if (!this.$config.profile.editorEnabled || !this.$config.profile.flags?.defaultPronoun) {
                return null;
            }
            let mainPronoun = buildPronoun(pronouns, this.$config.profile.flags?.defaultPronoun, this.$config, this.$translator);
            let mainOpinion = -1;
            for (const { pronoun, opinion } of this.pronounOpinions) {
                if (typeof pronoun === 'string') {
                    continue;
                }
                const opinionValue = opinions[opinion]?.value || 0;
                if (opinionValue > mainOpinion) {
                    mainPronoun = pronoun;
                    mainOpinion = opinionValue;
                }
            }

            return mainPronoun;
        },
    },
});
