import type { Config } from '../locale/config.ts';
import type { Translations } from '../locale/translations.ts';

export interface PronounsData<M extends string[]> {
    key: string;
    description: string;
    normative: string;
    [morpheme: M]: string | null;
    plural: string;
    pluralHonorific: string;
    pronounceable: string;
    history?: string;
    thirdForm?: M;
    smallForm?: M;
    sourcesInfo?: string;
    hidden?: boolean;
}

export interface PronounExamplesData {
    singular: string;
    plural?: string;
    isHonorific?: boolean;
}

declare namespace Data {
    declare module '*/config.suml' {
        declare const config: Config;
        export default config;
    }

    declare module '*/translations.suml' {
        declare const translations: Translations;
        export default translations;
    }

    declare module '*/pronouns/pronouns.tsv' {
        declare const data: PronounsData[];
        export default data;
    }

    declare module '*/pronouns/examples.tsv' {
        declare const data: PronounExamplesData[];
        export default data;
    }

    declare module '*.tsv' {
        declare const data: Record<string, any>[];
        export default data;
    }
}
