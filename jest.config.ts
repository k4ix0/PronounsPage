import type { Config } from 'jest';

const config: Config = {
    extensionsToTreatAsEsm: ['.ts'],
    moduleNameMapper: {
        '^(\\.{1,2}/.*)\\.js$': '$1',
        // need to manually reference the non-ESM versions of vue packages
        '^vue$': '<rootDir>/node_modules/vue/dist/vue.common.js',
        '^@vue/test-utils$': '<rootDir>/node_modules/@vue/test-utils/dist/vue-test-utils.js',
    },
    testEnvironment: 'node',
    testPathIgnorePatterns: ['.yarn', 'new', 'node_modules'],
    transform: {
        '^.+\\.ts$': ['ts-jest', {
            // use isolated modules to speed up test execution; type checking happens separately
            isolatedModules: true,
            useESM: true,
        }],
    },
};

export default config;
