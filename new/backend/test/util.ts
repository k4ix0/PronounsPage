import { getConfig, validateConfig } from "#self/config";
import log from "#self/log";
import { exit } from "node:process";
import { getActiveLocaleDescriptions, loadAllLocales, loadLocaleDescriptions } from "#self/locales";
import { loadStaticData } from "#self/util/static";

export async function prepareConfig() {
    try {
        log.level = "error"; // Silences logs
        const validationError = validateConfig(getConfig());
        if (validationError != undefined) {
            log.error(`Configuration is invalid: ${validationError}`);
            exit(1);
        }
    } catch (e) {
        log.error(`Could not validate configuration: ${e}`);
        exit(1);
    }
}
export async function prepareLocales() {
    try {
        const localeDescriptions = await loadLocaleDescriptions();
        const active = getActiveLocaleDescriptions();
        await loadAllLocales();
        log.info(
            `${localeDescriptions.length} locale(s) are registered, of which ${active.length} are active`
        );
    } catch (e) {
        log.error(`Could not load locales: ${e}`);
        exit(1);
    }
}

export async function prepareStaticData() {
    try {
        await loadStaticData();
        log.info("Loaded static data");
    } catch (e) {
        log.error(`Could not load static data: ${e}`);
        exit(1);
    }
}

export async function prepareTestEnvironment() {
    await prepareConfig();
    await prepareLocales();
    await prepareStaticData();
}