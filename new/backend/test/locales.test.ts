import { prepareConfig } from "./util.ts";
import { getConfig } from "#self/config";
import {
    getActiveLocaleDescriptions, getExpectedTranslations, getLocale,
    getLocaleDescriptions,
    loadAllLocales,
    loadLocaleDescriptions,
    Locale,
    LocaleDescription,
    refreshLocaleDescriptionCaches,
} from "#self/locales";

beforeAll(() => loadLocaleDescriptions());

describe("locale descriptions", () => {
    it("loadLocaleDescriptions", async () => {
        await loadLocaleDescriptions(true);
    });

    const compareDescriptions = (a: LocaleDescription, b: LocaleDescription) => a.code.localeCompare(b.code)
    it("active descriptions (allowUnpublished = true)", () => {
        getConfig().locale.allowUnpublished = true;
        refreshLocaleDescriptionCaches();
        const available = getLocaleDescriptions();
        const correct = available.filter((v) => !v.pseudo).sort(compareDescriptions);

        expect(getActiveLocaleDescriptions().sort(compareDescriptions)).toEqual(correct);
    });
    it("active descriptions (allowUnpublished = false)", () => {
        getConfig().locale.allowUnpublished = false;
        refreshLocaleDescriptionCaches();
        const available = getLocaleDescriptions();
        const correct = available.filter((v) => v.published).sort(compareDescriptions);

        expect(getActiveLocaleDescriptions().sort(compareDescriptions)).toEqual(correct);
    })
})

describe("locales", () => {
    beforeAll(() => {
        getConfig().locale.allowUnpublished = false;
        refreshLocaleDescriptionCaches();
        return loadAllLocales();
    })
    it("loadAllLocales", () => {
        return loadAllLocales();
    })
    
    function allLocales(f: (locale: Locale) => void) {
        const locales = getLocaleDescriptions();
        for (const desc of locales) {
            const locale = getLocale(desc.code);
            if (locale != null) {
                f(locale)
            }
        }
    }

    it("expected translations", () => {
        const expectedKeys = getExpectedTranslations();
        allLocales((locale) => {
            for (const key of expectedKeys) {
                const value = locale.getTranslation(key);
                expect(value).toBeDefined();
                expect(typeof value).toEqual("string");
            }
        });
    });

    it("toJSON", () => {
        allLocales((locale) => {
            const json = locale.toJSON();
            expect(json["code"]).toEqual(locale.code);
            expect(json["fallbackLocale"]).toEqual(locale.fallbackLocale);
            expect(json["config"]).toEqual(locale.config);
            expect(json["logo"]).toEqual(locale.logo);
            expect(json["logoSource"]).toEqual(locale.logoSource);
        });
    })
})