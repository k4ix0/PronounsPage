import fastify from "fastify";
import log from "#self/log";
import { getConfig } from "#self/config";
import v1 from "#self/server/v1";
import fastifyCookiePlugin from "@fastify/cookie";

export async function startServer() {
    const config = getConfig();
    const app = fastify({
        logger: log,
        ajv: {
            customOptions: {
                coerceTypes: "array",
            },
        },
    });

    app.register(fastifyCookiePlugin, {});

    let start: Date;

    app.get("/ping", (req, reply) => {
        reply.send({
            message: `Pong! The server is running!`,
            status: {
                uptime: Date.now() - start.getTime(),
            },
        });
    });
    app.register(v1, { prefix: "/v1" });

    try {
        await app.listen({
            host: config.http.host,
            port: config.http.port,
        });
        start = new Date(Date.now());
        log.info(`(Public base URL: ${config.http.baseUrl})`);
    } catch (e) {
        log.error(e);
        return;
    }
}
