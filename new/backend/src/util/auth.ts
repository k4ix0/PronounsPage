import { db, User } from "#self/db";
import { findAuthenticatorsByUser } from "#self/db/authenticator";
import jwt from "#self/util/jwt";
import { getAvatarUrlForUser } from "#self/util/avatar";
import { ExtendedUser } from "#self/db/user";

export interface UserJwt {
    id: string;
    username: string;
    email: string;
    roles: string;
    avatarSource: string | null;
    bannedReason: string | null;

    /* URL to the user's avatar. */
    avatar: string;
    mfa: boolean;
    mfaRequired: boolean;
    authenticated: boolean;
}
export function isUserJwt(value: unknown): value is UserJwt {
    if (value == null || typeof value !== "object") {
        return false;
    }
    const keys = [
        ["id", "string"],
        ["username", "string"],
        ["email", "string"],
        ["roles", "string"],
        ["avatar", "string"],
        ["mfa", "boolean"],
        ["mfaRequired", "boolean"],
        ["authenticated", "boolean"],
    ] as const;
    for (const [key, type] of keys) {
        if (!(key in value)) {
            return false;
        }
        if (typeof (value as any)[key] === type) {
            return false;
        }
    }

    return true;
}
export function isAuthenticatedUserJwt(
    value: unknown
): value is UserJwt & { authenticated: true } {
    if (!isUserJwt(value)) {
        return false;
    }
    return value.authenticated;
}

export function getUserFromJwt(token: UserJwt): Promise<User | null> {
    return db.user.findFirst({
        where: {
            id: token.id,
        },
    });
}

export function createUserJwt(user: ExtendedUser, guardMfa: boolean): UserJwt {
    const mfaRequired = user.hasMfa ? guardMfa : false;
    return {
        id: user.id,
        username: user.username,
        email: user.email,
        roles: user.roles,
        bannedReason: user.bannedReason,
        avatarSource: user.avatarSource,
        avatar: user.avatarUrl,
        mfa: user.hasMfa,
        mfaRequired,
        authenticated: !mfaRequired,
    };
}
export function issueUserJwt(user: ExtendedUser, guardMfa: boolean): string {
    const userJwt = createUserJwt(user, guardMfa);
    return jwt.signPayload(userJwt);
}
