import fsp from "node:fs/promises";
import { getConfig } from "#self/config";
import * as path from "node:path";

export interface StaticLogo {
    primary: string;
    light: string;
    colorless: string;
}
export interface StaticData {
    logo: StaticLogo;
    logoSource: StaticLogo;
}

let staticData: StaticData;

async function readFile(file: string): Promise<string>;
async function readFile<T>(file: string, parse: (data: Buffer) => T): Promise<string>;
async function readFile<T>(file: string, parse?: (data: Buffer) => T | string): Promise<T | string> {
    parse ??= (v) => v.toString('utf-8');
    return parse(await fsp.readFile(path.resolve(getConfig().static.dataPath, file)));
}

export async function loadStaticData(): Promise<StaticData> {
    const logoSource: StaticLogo = {
        primary: await readFile('logo/logo-primary.svg'),
        light: await readFile('logo/logo-light.svg'),
        colorless: await readFile('logo/logo.svg')
    }
    const logo = {
        primary: `data:image/svg+xml,${encodeURIComponent(logoSource.primary)}`,
        light: `data:image/svg+xml,${encodeURIComponent(logoSource.light)}`,
        colorless: `data:image/svg+xml,${encodeURIComponent(logoSource.colorless)}`
    }

    staticData = {
        logoSource,
        logo
    };
    return staticData;
}
export function getStaticData(): StaticData {
    return staticData;
}