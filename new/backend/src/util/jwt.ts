import JWT from "jsonwebtoken";
import { getPrivateKey, getPublicKey, loadSecretKey } from "#self/util/keys";
import { getConfig } from "#self/config";
import { getActiveLocaleDescriptions } from "#self/locales";
import log from "#self/log";

export interface UserTokenPayload {}

const ALGORITHM: JWT.Algorithm = "RS256" as const;
export const DEFAULT_EXPIRY = "365d";

function getAudience() {
    return getActiveLocaleDescriptions().map((v) => v.url);
}

export type ValidJWTPayload = string | object | Buffer;

export function signPayload<T extends ValidJWTPayload = any>(
    payload: T,
    expiresIn: string | number = DEFAULT_EXPIRY
): string {
    const config = getConfig();
    return JWT.sign(payload, getPrivateKey(), {
        expiresIn,
        algorithm: ALGORITHM,
        audience: getAudience(),
        issuer: config.http.baseUrl,
    });
}
export function validateToken<T extends ValidJWTPayload = any>(
    token: string
): (T & JWT.JwtPayload) | null {
    try {
        return JWT.verify(token, getPublicKey(), {
            algorithms: [ALGORITHM],
            audience: getAudience(),
            issuer: getConfig().http.baseUrl,
        }) as T & JWT.JwtPayload;
    } catch (e) {
        log.debug(`JWT token validation failed: ${e}`);
        return null;
    }
}

export default { signPayload, validateToken };
