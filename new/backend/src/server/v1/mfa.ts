import { ApiError, localeSpecific, replyError } from "#self/server/v1";
import { issueUserJwt, isUserJwt } from "#self/util/auth";
import { Type } from "@sinclair/typebox";
import {
    AuthenticatorType,
    findAuthenticatorByUser,
    findAuthenticatorsByUser,
} from "#self/db/authenticator";
import { getUserById } from "#self/db/user";
import { Cookie, cookieSettings } from "#self/util/cookie";
import { Environment, getConfig } from "#self/config";
import log from "#self/log";
import { verify } from "#self/util/mfa";

export const plugin = async function (instance) {
    instance.post(
        "/:locale/mfa/validate",
        {
            schema: {
                params: localeSpecific,
                body: Type.Object({
                    code: Type.String(),
                    recovery: Type.Optional(Type.Boolean()),
                }),
            },
        },
        async (req, reply) => {
            const config = getConfig();
            if (req.v1.token == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }
            const payload = req.v1.token.payload;
            if (!isUserJwt(payload)) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }
            if (!payload.mfaRequired) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            const user = await getUserById(payload.id);
            if (user == null) {
                return replyError(reply, ApiError.UNAUTHORIZED);
            }

            const code = req.body.code.trim();
            if (req.body.recovery) {
                const authenticators = await findAuthenticatorsByUser(
                    user.id,
                    AuthenticatorType.MultifactorRecovery
                );
                for (const authenticator of authenticators) {
                    if (authenticator.payload === code) {
                        // TODO: disable MFA
                        const token = await issueUserJwt(user, false);

                        // TODO: audit log, auth/mfa_recovery_successful
                        return reply
                            .setCookie(Cookie.Token, token, cookieSettings())
                            .send({ token });
                    }
                }
            }

            const authenticator = await findAuthenticatorByUser(
                user.id,
                AuthenticatorType.MultifactorSecret
            );
            if (authenticator == null) {
                log.warn(
                    "Validating MFA code failed because the user does not have MFA enabled; should not be possible."
                );
                return replyError(reply, ApiError.MFA_NOT_AVAILABLE);
            }

            // TODO: Login attempts

            let codeIsValid = false;
            if (
                config.environment === Environment.DEVELOPMENT &&
                code === "999999"
            ) {
                codeIsValid = true;
            } else {
                codeIsValid = verify(code, authenticator.payload);
            }

            if (!codeIsValid) {
                return replyError(reply, ApiError.MFA_CODE_INVALID);
            }

            const token = await issueUserJwt(user, false);

            reply.setCookie(Cookie.Token, token);
            return reply.send({ token });
        }
    );
} satisfies AppPluginAsync;
export default plugin;
