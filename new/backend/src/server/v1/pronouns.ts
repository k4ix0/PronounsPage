import {
    localeSpecific,
    ApiError,
    replyError,
    transformPronoun,
} from "#self/server/v1";
import {
    getLocale,
    parsePronounFromParts,
    parsePronounFromString,
    PronounExample,
} from "#self/locales";
import { isNotBlank, parseBool } from "@pronounspage/common/util";
import { Type } from "@sinclair/typebox";

export const plugin = async function (app: AppInstance) {
    // TODO: "Memoize" this route to reduce memory usage
    app.get(
        "/:locale/pronouns",
        {
            schema: {
                params: localeSpecific,
            },
        },
        (request, reply) => {
            const locale = getLocale(request.params.locale);
            if (locale === null) {
                return replyError(reply, ApiError.INVALID_LOCALE);
            }

            const obj: Record<string, unknown> = {};
            for (const pronoun of locale.pronouns) {
                obj[pronoun.keys[0]] = transformPronoun(
                    pronoun,
                    locale.examples,
                    locale
                );
            }

            return obj;
        }
    );

    function parseExample(s: string): PronounExample {
        const [singular, plural, isHonorific] = s.split("|");
        return {
            singular: isNotBlank(singular) ? singular : undefined,
            plural: isNotBlank(plural) ? plural : undefined,
            isHonorific: isNotBlank(isHonorific) && parseBool(isHonorific),
        };
    }

    // TODO: See previous TODO. Although maybe not with query parameters.
    app.get(
        "/:locale/pronouns/*",
        {
            schema: {
                params: Type.Intersect([
                    localeSpecific,
                    Type.Object({ "*": Type.String() }),
                ]),
                querystring: Type.Object({
                    "examples[]": Type.Array(Type.String(), { default: [] }),
                }),
            },
        },
        (req, reply) => {
            const locale = getLocale(req.params.locale);
            if (locale == null) {
                return replyError(reply, ApiError.INVALID_LOCALE);
            }

            const keyParts: Array<string> = req.params["*"]
                .split("/")
                .filter(isNotBlank);
            const key = keyParts.join("/");
            let found = locale.pronoun(key);
            if (found == null) {
                found = parsePronounFromParts(keyParts, locale.morphemes);
                // req.log.info(`${JSON.stringify(keyParts)} - ${found}`);
            }
            if (found == null) {
                return replyError(reply, ApiError.UNKNOWN_PRONOUN);
            }

            const examples = req.query["examples[]"];

            return transformPronoun(
                found,
                examples.length < 1
                    ? locale.examples
                    : examples.map(parseExample),
                locale,
                {
                    processName: true,
                    isUserGenerated: true,
                }
            );
        }
    );
} satisfies AppPluginAsync;
export default plugin;
