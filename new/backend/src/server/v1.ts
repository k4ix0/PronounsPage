import { Type } from "@sinclair/typebox";
import {
    allPronounVariants,
    examplesFor,
    Locale,
    Pronoun,
    PronounExample,
} from "#self/locales";
import { authenticateV1 } from "#self/server/util/v1";
import pronouns from "#self/server/v1/pronouns";
import inclusive from "#self/server/v1/inclusive";
import mfa from "#self/server/v1/mfa";
import user from "#self/server/v1/user";
import { UserJwt } from "#self/util/auth";
import { ValidJWTPayload } from "#self/util/jwt";

export type V1AppInstance = AppInstance;

export type TokenData<T = ValidJWTPayload> = {
    method: "cookie" | "header";
    payload: T;
};
export type V1Data = {
    token: TokenData | null;
};

export interface ApiErrorOptions {
    /* HTTP status code. */
    code: number;
    /* Error ID. Should be used for localising if that's added. */
    id: string;
    /* Error message. Should be reasonably descriptive. */
    message: string;
    /* Details. May optionally be provided to describe more. */
    detail?: string;
}

/**
 * Creates an error object for the v1 API endpoints.
 * This function may seem a bit extraneous but it's a useful utility for consistency.
 * @param options
 */
export function error<T extends ApiErrorOptions>(options: T) {
    const response: Record<string, unknown> = {
        code: options.code,
        message: options.message,
    };
    if (options.detail) {
        response.detail = options.detail;
    }
    return response;
}

export function replyError<T extends ApiErrorOptions>(
    reply: AppReply,
    options: T
): AppReply {
    const response = error(options);
    return reply.status(options.code).send(response);
}

export const ApiError = {
    BAD_REQUEST: {
        code: 400,
        id: "generic.bad_request",
        message: "Bad request",
        detail: "The request you've sent is malformed or otherwise invalid according to our code.",
    },
    UNAUTHORIZED: {
        code: 401,
        id: "generic.unauthorized",
        message: "You are not authorized to perform this action.",
    },
    INTERNAL_ERROR: {
        code: 500,
        id: "generic.internal",
        message: "Internal server error",
        detail: "Something went wrong on our end. Contact the team about this.",
    },
    INVALID_LOCALE: {
        code: 404,
        id: "locale.invalid",
        message: "Invalid locale",
    },
    UNKNOWN_PRONOUN: {
        code: 404,
        id: "locale.unknown_pronoun",
        message:
            "We aren't aware of any such pronoun. Perhaps there's a typo, or perhaps you specified an incorrect number of morphemes?",
        detail: "Note that for custom pronouns, you need to specify all necessary forms: https://en.pronouns.page/faq#custom-pronouns",
    },
    USER_NOT_FOUND: {
        code: 404,
        id: "user.not_found",
        message: "We couldn't find any such user. Perhaps there's a typo?",
    },
    INVALID_EMAIL: {
        code: 400,
        id: "email.invalid",
        message: "This user's email is invalid.",
        detail: "If you believe this to be an error, contact us: https://en.pronouns.page/contact",
    },
    MFA_NOT_AVAILABLE: {
        code: 400,
        id: "mfa.not_available",
        message: "This user does not have multi-factor authentication enabled.",
    },
    MFA_CODE_INVALID: {
        code: 401,
        id: "mfa.code.invalid",
        message: "The code is invalid.",
    },
} satisfies Record<string, ApiErrorOptions>;

export const localeSpecific = Type.Object({
    locale: Type.String(),
});

export interface TransformPronounOptions {
    processName: boolean;
    isUserGenerated: boolean;
}
export function transformPronoun(
    pronoun: Pronoun,
    examples: Array<PronounExample>,
    locale: Locale,
    options?: Partial<TransformPronounOptions>
) {
    const opts: TransformPronounOptions = {
        processName: options?.processName ?? false,
        isUserGenerated: options?.isUserGenerated ?? false,
    };
    const morphemes: Record<string, string> = {};
    const pronunciations: Record<string, string> = {};
    for (const [name, form] of Object.entries(pronoun.forms)) {
        morphemes[name] = allPronounVariants(form)
            .map((v) => v.written)
            .join("&");
        pronunciations[name] = form.pronounced;
    }
    return {
        canonicalName: pronoun.canonicalName,
        description: pronoun.description ?? "",
        normative: pronoun.isNormative,
        morphemes,
        pronunciations,
        plural: [pronoun.isPlural],
        pluralHonorific: [pronoun.isPluralHonorific],
        aliases: pronoun.keys.slice(1),
        history: opts.isUserGenerated ? "__generator__" : pronoun.history ?? "",
        pronounceable: pronoun.isPronounceable,
        thirdForm: pronoun.thirdForm ?? null,
        smallForm: pronoun.smallForm ?? null,
        sourcesInfo: pronoun.source ?? null,
        examples: examplesFor(pronoun, examples),
        name: opts.processName ? locale.pronounName(pronoun) : undefined,
    };
}

/*
 * Version 1 of the API. This would be the "legacy" version.
 * Since the output of every endpoint is locale-specific,
 * every route is prefixed with the respective locale ID.
 */

export const routes = async function (app: AppInstance) {
    app.decorateRequest("v1", null);
    app.addHook("onRequest", (req, _, done) => {
        req.v1 = {
            token: null,
        };
        done();
    });
    app.register(authenticateV1);
    app.register(pronouns);
    app.register(inclusive);
    app.register(mfa);
    app.register(user);
} satisfies AppPluginAsync;
export default routes;
