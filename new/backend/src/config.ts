import "dotenv/config";
import {
    identity,
    parseBool,
    parseInteger,
    toLowerCase,
} from "@pronounspage/common/util";
import * as path from "node:path";
import * as fs from "node:fs";
import type { log } from "#self/log";

export enum Environment {
    DEVELOPMENT = "dev",
    PRODUCTION = "production",
}

export interface Config {
    environment: Environment;
    logLevel?: (typeof log)["level"];
    http: {
        baseUrl: string;
        host: string;
        port: number;
    };
    database: {
        url: string;
    };
    mailer: {
        transport?: string;
        fromAddress: string;
        overwrite?: string;
    };
    security: {
        secret: string;
        // NOTE(tecc): In the future, maybe we allow different keys for different purposes
        //             (e.g. backups have one key, JWTs have another, etc.)
        publicKey: string;
        privateKey: string;
        allowGenerateKeys: boolean;
    };
    locale: {
        dataPath: string;
        fallbackLocale: string;
        allowUnpublished: boolean;
    };
    turnstile: {
        secret: string;
        sitekey: string;
    };
    static: {
        dataPath: string;
    }
}

function envVarOrDefault<T>(
    key: string,
    parse: (value: string) => T,
    defaultValue: T
): T;
function envVarOrDefault<T>(
    key: string,
    parse: (value: string) => T,
    defaultValue?: undefined
): T | undefined;
function envVarOrDefault<T>(
    key: string,
    parse: (value: string) => T,
    defaultValue: T | undefined = undefined
): T | undefined {
    const value = process.env[key];
    return value == null ? defaultValue : parse(String(value));
}

function envVarThen<T>(
    key: string,
    parse: (value: string) => T,
    f: () => T
): T {
    const value = envVarOrDefault(key, parse);
    return value == undefined ? f() : value;
}

function envVarNotNull<T>(key: string, parse: (value: string) => T): T {
    const value = envVarOrDefault(key, parse);
    if (value === undefined) {
        throw new Error(`Environment variable ${key} is missing`);
    }
    return value;
}

function parseEnvironment(value: string): Environment {
    switch (value.toLowerCase()) {
        case "dev":
        case "development":
            return Environment.DEVELOPMENT;
        case "prod":
        case "production":
            return Environment.PRODUCTION;
        default:
            throw new Error(`"${value}" is not a valid environment`);
    }
}

function parsePath(value: string): string {
    return path.resolve(process.cwd(), value);
}

export function loadConfigFromEnv(): Config {
    return {
        environment: envVarOrDefault(
            "ENVIRONMENT",
            parseEnvironment,
            Environment.DEVELOPMENT
        ),
        logLevel: envVarOrDefault("LOG_LEVEL", toLowerCase, undefined),
        http: {
            baseUrl: envVarNotNull("HTTP_BASE_URL", identity),
            host: envVarOrDefault("HTTP_HOST", identity, "0.0.0.0"),
            port: envVarOrDefault("HTTP_PORT", parseInteger, 4000),
        },
        database: {
            url: envVarNotNull("DATABASE_URL", identity),
        },
        mailer: {
            transport: envVarOrDefault("MAILER_TRANSPORT", identity, undefined),
            fromAddress: envVarOrDefault("MAILER_FROM", identity, "contact@pronouns.page"),
            overwrite: envVarOrDefault("MAILER_OVERWRITE", identity, undefined),
        },
        security: {
            secret: envVarNotNull("SECURITY_SECRET", identity),
            publicKey: envVarNotNull("SECURITY_PUBLIC_KEY", parsePath),
            privateKey: envVarNotNull("SECURITY_PRIVATE_KEY", parsePath),
            allowGenerateKeys: envVarOrDefault("SECURITY_ALLOW_GENERATE_KEYS", parseBool, false)
        },
        locale: {
            dataPath: envVarOrDefault(
                "LOCALE_DATA_PATH",
                parsePath,
                "../locales"
            ),
            fallbackLocale: envVarOrDefault("LOCALE_FALLBACK", identity, "_base"),
            allowUnpublished: envVarThen(
                "LOCALE_ALLOW_UNPUBLISHED",
                parseBool,
                () =>
                    envVarOrDefault(
                        "ALLOW_UNPUBLISHED_LOCALES",
                        parseBool,
                        false
                    )
            ),
        },
        turnstile: {
            sitekey: envVarOrDefault(
                "TURNSTILE_SITEKEY",
                identity,
                "1x00000000000000000000AA"
            ),
            secret: envVarOrDefault(
                "TURNSTILE_SECRET",
                identity,
                "1x00000000000000000000AA"
            ),
        },
        static: {
            dataPath: envVarOrDefault("STATIC_DATA_PATH", parsePath, "../static")
        }
    };
}

export function validateConfig(config: Config): string | undefined {
    if (!fs.existsSync(config.locale.dataPath)) {
        return `Locale data path is set to ${config.locale.dataPath}, but it does not exist`;
    }
    if (!fs.existsSync(config.static.dataPath)) {
        return `Static data path is set to ${config.static.dataPath}, but it does not exist`;
    }
    // I mean these two next ones really aren't necessary, but I find it funny
    // ...who said we can't have a little fun here and there?
    const secretLower = config.security.secret.toLowerCase();
    if (secretLower === "changeme") {
        return `You didn't change the secret? The secret is quite literally "${secretLower}"`;
    }
    if (
        secretLower === "changemeonprod!" &&
        config.environment === Environment.PRODUCTION
    ) {
        return `Hey now, this is production! You have to change the secret on prod! Look, it says so: ${config.security.secret}`;
    }

    return undefined;
}

let loadedConfig: Config | undefined = undefined;

export function getConfig() {
    if (loadedConfig == undefined) {
        loadedConfig = loadConfigFromEnv();
    }
    return loadedConfig;
}
